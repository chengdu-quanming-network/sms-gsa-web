function randomNum(minNum,maxNum){ 
    switch(arguments.length){ 
        case 1: 
            return parseInt(Math.random()*minNum+1,10); 
        break; 
        case 2: 
            return parseInt(Math.random()*(maxNum-minNum+1)+minNum,10); 
        break; 
            default: 
                return 0; 
            break; 
    } 
}

function put_out(mess){
	document.getElementById('put_out').append(mess + "\n");
}

function one_send(tel_number, open_id, api_key, modelId, args){
	// var open_id = String(document.getElementById('open_id').value)
	// var api_key = String(document.getElementById('apiKey').value)
	// var modelId = String(document.getElementById('model_id').value)
	// var args = String(document.getElementById('set_args').value)
	
	
	// var tel_number = document.getElementById('tel_s').value
	put_out('[send]to ' + tel_number)
	
	console.log(open_id)
	console.log(api_key)
	console.log(modelId)
	
	$.post("http://dev.quanmwl.com/v1/sms",
	{
	  openID: open_id,
	  tel: tel_number,
	  // {openID}{apiKey}{tel}{model_id}{model_args}
	  sign: md5(open_id+api_key+tel_number+modelId+args),
	  model_id: modelId,
	  model_args: args
	},
	function(data,status){
		console.log("数据: \n" + data + "\n状态: " + status);
		
		console.log(data);
		console.log(data.state);
		// if(j_data.state=='200')alert('546666');
		if(data.state=='200'){
			console.log('发送成功');
			put_out('[send]' + tel_number + '成功')
			return true
		}else{
			console.log(data);
			if(data.state!=null){
				// alert('提示：' + data.mess)
				put_out('[send]' + tel_number + '失败：' + data.mess)
			}else{
				// alert('发送失败，请检查后重试')
				put_out('[send]' + tel_number + '失败：不明')
			}
			return false
		};
	}).error(function(){
		//异常处理
		alert('服务器繁忙，请稍后再试')
		put_out('[server]服务器繁忙')
		return false
		}
	);
}

function start_all(){
	var all_tel = String(document.getElementById('tel_s').value).split('\n')
	console.log(all_tel)
	
	var max_number = all_tel.length
	localStorage.setItem('temp_number', 0)
	document.getElementById('jdt').max=max_number
	document.getElementById('jdt').value=0
	
	
	// 避免重复获取
	var open_id = String(document.getElementById('open_id').value)
	var api_key = String(document.getElementById('apiKey').value)
	var modelId = String(document.getElementById('model_id').value)
	var args = String(document.getElementById('set_args').value)
	
	// 重置本地缓存
	localStorage.setItem('oid', open_id)
	localStorage.setItem('apiKey', api_key)
	localStorage.setItem('modelId', modelId)
	localStorage.setItem('model_args', args)
	
	for (i in all_tel) {
		var one_tel = all_tel[i]
		one_send(one_tel, open_id, api_key, modelId, args)
		var old_number = localStorage.getItem('temp_number')
		var new_number = old_number + 1
		document.getElementById('jdt').value=new_number
		localStorage.setItem('temp_number', new_number)
	}
}

function load_local_data(){
	var open_id = localStorage.getItem('oid')
	if(open_id!=null){
		
		var api_key = localStorage.getItem('apiKey')
		var model_Id = localStorage.getItem('modelId')
		var model_args = localStorage.getItem('model_args')
		
		document.getElementById('open_id').value=open_id
		document.getElementById('apiKey').value=api_key
		document.getElementById('model_id').value=model_Id
		document.getElementById('set_args').value=model_args
	}
}
